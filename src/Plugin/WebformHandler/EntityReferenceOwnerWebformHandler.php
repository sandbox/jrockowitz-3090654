<?php

namespace Drupal\webform_entity_reference_owner\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user\EntityOwnerInterface;
use Drupal\webform\Plugin\WebformElementEntityReferenceInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Changes the owner of an entity (node) reference to the webform submission owner.
 *
 * @WebformHandler(
 *   id = "entity_reference_owner",
 *   label = @Translation("Entity reference owner"),
 *   category = @Translation("Entity references"),
 *   description = @Translation("Changes the owner of an entity (node) reference to the webform submission's owner."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED
 * )
 */
class EntityReferenceOwnerWebformHandler extends WebformHandlerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'element_key' => '',
      'default_uid' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    $element = $this->getWebform()->getElement($settings['element_key']);
    if ($element) {
      $webform_element = $this->elementManager->getElementInstance($element);
      $t_args = [
        '@title' => $webform_element->getAdminLabel($element),
        '@type' => $webform_element->getPluginLabel(),
      ];
      $settings['element_key'] = $this->t('@title (@type)', $t_args);
    }
    elseif (empty($settings['element_key'])) {
      $settings['element_key'] = [
        '#type' => 'link',
        '#title' => $this->t('Please add a new options elements.'),
        '#url' => $this->getWebform()->toUrl('edit-form'),
      ];
    }
    else {
      $settings['element_key'] = [
        '#markup' => $this->t("'@element_key' is missing.", ['@element_key' => $settings['element_key']]),
        '#prefix' => '<b class="color-error">',
        '#suffix' => '</b>',
      ];
    }

    $account = User::load($settings['default_uid']);
    if ($account) {
      $t_args = [
        '@label' => $account->label(),
        '@uid' => $account->id(),
      ];
      $settings['default_uid'] = $this->t('@label (@uid)', $t_args);
    }
    else {
      $settings['default_uid'] = [
        '#markup' => $this->t("'@uid' is missing.", ['@uid' => $settings['default_uid']]),
        '#prefix' => '<b class="color-error">',
        '#suffix' => '</b>',
      ];
    }

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['entity_reference'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity reference settings'),
    ];
    $form['entity_reference']['element_key'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity reference element'),
      '#options' => $this->getEntityReferenceElements(),
      '#default_value' => $this->configuration['element_key'],
      '#required' => TRUE,
    ];
    $form['entity_reference']['default_uid'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Default owner'),
      '#description' => $this->t("Default owner is used when an entity reference element's value is changed."),
      '#target_type' => 'user',
      '#settings' => [
        'match_operator' => 'CONTAINS',
      ],
      '#selection_settings' => [
        'include_anonymous' => TRUE,
      ],
      '#default_value' => User::load($this->configuration['default_uid']) ?: 0,
      '#required' => TRUE,
    ];
    $this->setSettingsParentsRecursively($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['element_key'] = $form_state->getValue('element_key');
    $this->configuration['default_uid'] = $form_state->getValue('default_uid');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(WebformSubmissionInterface $webform_submission) {
    $element_key = $this->configuration['element_key'];
    $default_uid = $this->configuration['default_uid'];

    $element = $this->getWebform()->getElement($this->configuration['element_key']);
    if (!$element) {
      return;
    }

    // Get the original and current entity reference ids.
    $original_entity_ids = (array) $webform_submission->getElementOriginalData($element_key) ?: [];
    if ($original_entity_ids) {
      $original_entity_ids = array_combine($original_entity_ids, $original_entity_ids);
      ksort($original_entity_ids);
    }
    $entity_ids = (array) $webform_submission->getElementData($element_key) ?: [];
    if ($entity_ids) {
      $entity_ids = array_combine($entity_ids, $entity_ids);
      ksort($entity_ids);
    }
    // Don't change the owner if entity (reference) id has not changed.
    if ($original_entity_ids == $entity_ids) {
      return;
    }

    $entity_storage = $this->entityTypeManager->getStorage($element['#target_type']);

    // Reset original entity owner to the default uid.
    $default_entity_ids = array_diff_assoc($original_entity_ids, $entity_ids);
    if ($default_entity_ids) {
      $entities = $entity_storage->loadMultiple($default_entity_ids);
      foreach ($entities as $entity) {
        $entity->setOwnerId($default_uid);
        $entity->save();
      }
    }

    // Set the entity reference owner to submission owner.
    $owner_entity_ids = array_diff_assoc($entity_ids, $original_entity_ids);
    if ($owner_entity_ids) {
      $entities = $entity_storage->loadMultiple($owner_entity_ids);
      foreach($entities as $entity) {
        $entity->setOwnerId($webform_submission->getOwnerId());
        $entity->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preDelete(WebformSubmissionInterface $webform_submission) {
    $element_key = $this->configuration['element_key'];
    $default_uid = $this->configuration['default_uid'];

    $element = $this->getWebform()->getElement($this->configuration['element_key']);
    if (!$element) {
      return;
    }

    $entity_storage = $this->entityTypeManager->getStorage($element['#target_type']);

    // Set the entity reference owner to default uid.
    $entity_id = $webform_submission->getElementData($element_key);
    if ($entity_id) {
      $entity = $entity_storage->load($entity_id);
      if ($entity) {
        $entity->setOwnerId($default_uid);
        $entity->save();
      }
    }
  }

  /****************************************************************************/
  // Helper methods.
  /****************************************************************************/

  /**
   * Get key/value array of webform entity reference elements.
   *
   * @return array
   *   A key/value array of webform entity reference elements.
   */
  protected function getEntityReferenceElements() {
    $webform = $this->getWebform();
    $elements = $webform->getElementsInitializedAndFlattened();

    $options = [];
    foreach ($elements as $element_key => $element) {
      $webform_element = $this->elementManager->getElementInstance($element);
      $is_entity_reference = $webform_element instanceof WebformElementEntityReferenceInterface;
      if ($is_entity_reference && isset($element['#target_type'])) {
        $definition = $this->entityTypeManager->getDefinition($element['#target_type']);
        if ($definition->entityClassImplements(EntityOwnerInterface::class)) {
          $t_args = [
            '@title' => $webform_element->getAdminLabel($element),
            '@type' => $webform_element->getPluginLabel(),
          ];
          $options[$element_key] = $this->t('@title (@type)', $t_args);
        }
      }
    }

    return $options;
  }

}
