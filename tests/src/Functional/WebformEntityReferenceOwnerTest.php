<?php

namespace Drupal\Tests\webform_entity_reference_owner\Functional;

use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Webform entity reference owner handler test.
 *
 * @group webform_browser
 */
class WebformEntityReferenceOwnerTest extends WebformBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'node',
    'webform',
    'webform_options_limit',
    'webform_entity_reference_owner',
    'webform_entity_reference_owner_test',
  ];

  /**
   * A array of nodes.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  protected $nodes = [];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->createContentType(['type' => 'page']);
    $this->nodes[1] = $this->createNode();
    $this->nodes[2] = $this->createNode();
    $this->nodes[3] = $this->createNode();
  }

  /**
   * Test entity reference owner handler.
   */
  public function testEntityReferenceOwnerHandler() {
    $webform = Webform::load('test_entity_reference_owner');
    $account = $this->createUser();

    /**************************************************************************/

    // Check that all nodes are owned by the anonymous account.
    $this->assertEqual($this->nodes[1]->getOwnerId(), 0);
    $this->assertEqual($this->nodes[2]->getOwnerId(), 0);
    $this->assertEqual($this->nodes[3]->getOwnerId(), 0);

    // Login as an authenticated user.
    $this->drupalLogin($account);

    // Submit entity reference to node 1.
    // @see \Drupal\webform_entity_reference_owner\Plugin\WebformHandler\EntityReferenceOwnerWebformHandler::preSave
    $sid_1 = $this->postSubmission($webform, ['entity_reference' => $this->nodes[1]->id()]);
    $webform_submission_1 = WebformSubmission::load($sid_1);

    // Check that node 1 is owned by authenticated user.
    $this->reloadNode();
    $this->assertEqual($this->nodes[1]->getOwnerId(), $account->id());
    $this->assertEqual($this->nodes[2]->getOwnerId(), 0);
    $this->assertEqual($this->nodes[3]->getOwnerId(), 0);

    // Change the entity reference to node 2.
    // @see \Drupal\webform_entity_reference_owner\Plugin\WebformHandler\EntityReferenceOwnerWebformHandler::preSave
    $webform_submission_1->setElementData('entity_reference', $this->nodes[2]->id());
    $webform_submission_1->save();

    // Check that node 2 is now owned by authenticated user.
    $this->reloadNode();
    $this->assertEqual($this->nodes[1]->getOwnerId(), 0);
    $this->assertEqual($this->nodes[2]->getOwnerId(), $account->id());
    $this->assertEqual($this->nodes[3]->getOwnerId(), 0);

    // Submit entity reference to node 3.
    $sid_2 = $this->postSubmission($webform, ['entity_reference' => $this->nodes[3]->id()]);
    $webform_submission_2 = WebformSubmission::load($sid_2);

    // Check that node 3 is now owned by authenticated user.
    $this->reloadNode();
    $this->assertEqual($this->nodes[1]->getOwnerId(), 0);
    $this->assertEqual($this->nodes[2]->getOwnerId(), $account->id());
    $this->assertEqual($this->nodes[3]->getOwnerId(), $account->id());

    // Delete submission 2.
    // @see \Drupal\webform_entity_reference_owner\Plugin\WebformHandler\EntityReferenceOwnerWebformHandler::preDelete
    $webform_submission_2->delete();

    // Check that node 3 is now owned by anonymous user.
    $this->reloadNode();
    $this->assertEqual($this->nodes[1]->getOwnerId(), 0);
    $this->assertEqual($this->nodes[2]->getOwnerId(), $account->id());
    $this->assertEqual($this->nodes[3]->getOwnerId(), 0);
  }

  /**
   * Reload nodes.
   */
  protected function reloadNode() {
    /** @var \Drupal\node\NodeStorageInterface $node_storage */
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node_storage->resetCache();
    foreach ($this->nodes as $index => $node) {
      $this->nodes[$index] = $node_storage->load($node->id());
    }
  }

}
