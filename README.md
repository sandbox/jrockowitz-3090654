Webform Entity Reference Owner 8.x-1.x
---------------------------------

### About this Module

The Webform Entity Reference Owner module provides a webform handler that 
changes the owner of an entity reference to the webform submission owner.
